DistributionBaseDir := texmf-local
ifeq ($(OS),Windows_NT)
	DelCmd = del /s /q
	MkdirCmd = mkdir
	RmdirCmd = rmdir /s /q
	CopyCmd = copy
	DistrDocBaseDir := $(DistributionBaseDir)\\doc
	DistrDocDir := $(DistrDocBaseDir)\\local\\latex\\vsb
	DistrDocFigDir := $(DistrDocDir)\\fig
	DistrThemeBaseDir := $(DistributionBaseDir)\\tex
	DistrThemeDir := $(DistrThemeBaseDir)\\latex\\local\\latex\\vsb
	DistrThemeImgDir := $(DistrThemeDir)\\ThemeImg
else
	DelCmd = rm -rf
	MkdirCmd = mkdir -p
	RmdirCmd = rm -rf
	CopyCmd = cp -r
	DistrDocBaseDir := $(DistributionBaseDir)/doc
	DistrDocDir := $(DistrDocBaseDir)/local/latex/vsb
	DistrDocFigDir := $(DistrDocDir)/fig
	DistrThemeBaseDir := $(DistributionBaseDir)/tex
	DistrThemeDir := $(DistrThemeBaseDir)/latex/local/latex/vsb
	DistrThemeImgDir := $(DistrThemeDir)/ThemeImg
endif

GeneratedTestFiles := GeneratedTestCeetFrametitleCzech.pdf GeneratedTestCeetFrametitleEnglish.pdf\
	GeneratedTestCeetNoframetitleCzech.pdf GeneratedTestCeetNoframetitleEnglish.pdf\
	GeneratedTestCntFrametitleCzech.pdf GeneratedTestCntFrametitleEnglish.pdf\
	GeneratedTestCntNoframetitleCzech.pdf GeneratedTestCntNoframetitleEnglish.pdf\
	GeneratedTestEkfFrametitleCzech.pdf GeneratedTestEkfFrametitleEnglish.pdf\
	GeneratedTestEkfNoframetitleCzech.pdf GeneratedTestEkfNoframetitleEnglish.pdf\
	GeneratedTestEnetFrametitleCzech.pdf GeneratedTestEnetFrametitleEnglish.pdf\
	GeneratedTestEnetNoframetitleCzech.pdf GeneratedTestEnetNoframetitleEnglish.pdf\
	GeneratedTestFastFrametitleCzech.pdf GeneratedTestFastFrametitleEnglish.pdf\
	GeneratedTestFastNoframetitleCzech.pdf GeneratedTestFastNoframetitleEnglish.pdf\
	GeneratedTestFbiFrametitleCzech.pdf GeneratedTestFbiFrametitleEnglish.pdf\
	GeneratedTestFbiNoframetitleCzech.pdf GeneratedTestFbiNoframetitleEnglish.pdf\
	GeneratedTestFeiFrametitleCzech.pdf GeneratedTestFeiFrametitleEnglish.pdf\
	GeneratedTestFeiNoframetitleCzech.pdf GeneratedTestFeiNoframetitleEnglish.pdf\
	GeneratedTestFmtFrametitleCzech.pdf GeneratedTestFmtFrametitleEnglish.pdf\
	GeneratedTestFmtNoframetitleCzech.pdf GeneratedTestFmtNoframetitleEnglish.pdf\
	GeneratedTestFsFrametitleCzech.pdf GeneratedTestFsFrametitleEnglish.pdf\
	GeneratedTestFsNoframetitleCzech.pdf GeneratedTestFsNoframetitleEnglish.pdf\
	GeneratedTestHgfFrametitleCzech.pdf GeneratedTestHgfFrametitleEnglish.pdf\
	GeneratedTestHgfNoframetitleCzech.pdf GeneratedTestHgfNoframetitleEnglish.pdf\
	GeneratedTestIetFrametitleCzech.pdf GeneratedTestIetFrametitleEnglish.pdf\
	GeneratedTestIetNoframetitleCzech.pdf GeneratedTestIetNoframetitleEnglish.pdf\
	GeneratedTestIt4iFrametitleCzech.pdf GeneratedTestIt4iFrametitleEnglish.pdf\
	GeneratedTestIt4iNoframetitleCzech.pdf GeneratedTestIt4iNoframetitleEnglish.pdf\
	GeneratedTestVecFrametitleCzech.pdf GeneratedTestVecFrametitleEnglish.pdf\
	GeneratedTestVecNoframetitleCzech.pdf GeneratedTestVecNoframetitleEnglish.pdf\
	GeneratedTestVsbFrametitleCzech.pdf GeneratedTestVsbFrametitleEnglish.pdf\
	GeneratedTestVsbNoframetitleCzech.pdf GeneratedTestVsbNoframetitleEnglish.pdf

LaTeX := lualatex
MakeIndex := makeindex
Biber := biber
Zip := zip -r

all: clean theme doc examples distribution zip postbuildclean

theme:
	$(LaTeX) vsb.ins

doc:
	$(LaTeX) vsb.drv
	$(MakeIndex) -s gglo.ist -o vsb.gls vsb.glo
	$(Biber) vsb.bcf
	$(LaTeX) vsb.drv
	$(LaTeX) vsb.drv

examples: theme MinimalWorkingExampleCZ.pdf MinimalWorkingExampleEN.pdf SamplePresentationCZ.pdf SamplePresentationEN.pdf

distribution: theme doc examples
	$(MkdirCmd) $(DistrDocDir)
	$(MkdirCmd) $(DistrDocFigDir)
	$(CopyCmd) vsb.pdf $(DistrDocDir)
	$(CopyCmd) MinimalWorkingExampleCZ.pdf $(DistrDocDir)
	$(CopyCmd) MinimalWorkingExampleEN.pdf $(DistrDocDir)
	$(CopyCmd) SamplePresentationCZ.pdf $(DistrDocDir)
	$(CopyCmd) SamplePresentationEN.pdf $(DistrDocDir)
	$(CopyCmd) MinimalWorkingExampleCZ.tex $(DistrDocDir)
	$(CopyCmd) MinimalWorkingExampleEN.tex $(DistrDocDir)
	$(CopyCmd) SamplePresentationCZ.tex $(DistrDocDir)
	$(CopyCmd) SamplePresentationEN.tex $(DistrDocDir)
	$(CopyCmd) fig/* $(DistrDocFigDir)
	$(MkdirCmd) $(DistrThemeDir)
	$(MkdirCmd) $(DistrThemeImgDir)
	$(CopyCmd) beamercolorthemevsb.sty $(DistrThemeDir)
	$(CopyCmd) beamerfontthemevsb.sty $(DistrThemeDir)
	$(CopyCmd) beamerinnerthemevsb.sty $(DistrThemeDir)
	$(CopyCmd) beamerouterthemevsb.sty $(DistrThemeDir)
	$(CopyCmd) beamerthemevsb.sty $(DistrThemeDir)
	$(CopyCmd) ThemeImg/* $(DistrThemeImgDir)

zip: distribution
	$(Zip) vsbtheme $(DistrDocBaseDir) $(DistrThemeBaseDir)

tests: theme gentests $(GeneratedTestFiles)

gentests:
	$(LaTeX) GeneratedTests.ins

%.pdf: %.tex
	$(LaTeX) $<
	$(LaTeX) $<

clean: cleanwork
	$(DelCmd) beamercolorthemevsb.sty
	$(DelCmd) beamerfontthemevsb.sty
	$(DelCmd) beamerinnerthemevsb.sty
	$(DelCmd) beamerouterthemevsb.sty
	$(DelCmd) beamerthemevsb.sty
	$(DelCmd) vsb.pdf
	$(DelCmd) MinimalWorkingExampleCZ.pdf
	$(DelCmd) MinimalWorkingExampleEN.pdf
	$(DelCmd) SamplePresentationCZ.pdf
	$(DelCmd) SamplePresentationEN.pdf
	$(DelCmd) GeneratedTest*.tex
	$(DelCmd) GeneratedTest*.pdf
	-$(RmdirCmd) $(DistributionBaseDir)
	$(DelCmd) vsbtheme.zip

cleanwork: cwA

postbuildclean: cwB

cw%:
	$(DelCmd) *.bak
	$(DelCmd) *.aux
	$(DelCmd) *.log
	$(DelCmd) *.lot
	$(DelCmd) *.lof
	$(DelCmd) *.lol
	$(DelCmd) *.toc
	$(DelCmd) *.out
	$(DelCmd) *.glo
	$(DelCmd) *.gls
	$(DelCmd) *.ilg
	$(DelCmd) *.snm
	$(DelCmd) *.nav
	$(DelCmd) *.bbl
	$(DelCmd) *.bcf
	$(DelCmd) *.blg
	$(DelCmd) *.run.xml
